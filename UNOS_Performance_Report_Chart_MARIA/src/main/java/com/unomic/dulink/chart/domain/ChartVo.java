
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String jig;
	String WC;
	String GRNM;
	String targetDateTime;
	int inCycleTime;
	int cuttingTime;
	int waitTime;
	int alarmTime;
	int noConnectionTime;
	String line;
	String sDate;
	String eDate;
	
	//common
	Integer shopId;
	Integer dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
	
}
